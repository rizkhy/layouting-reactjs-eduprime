import { ArcElement, Chart as ChartJS, Legend, Tooltip } from "chart.js";
import React from "react";
import { Doughnut } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);

const ChartDonut = ({ chartDataVal, bgColorVal }) => {
    const chartData = chartDataVal;
    const bgColor = bgColorVal;
    const showData = chartData[0] + "%";
    const options1 = {
        responsive: true,
        legend: {
            display: false,
            position: "bottom",
            labels: {
                fontSize: 18,
                fontColor: "#6D7278",
                fontFamily: "kanit light",
            },
        },
    };

    const data = {
        labels: ["", ""],
        datasets: [
            {
                data: chartData,
                backgroundColor: bgColor,
            },
        ],
        text: showData,
    };

    return (
        <div>
            <Doughnut data={data} options={options1} />
        </div>
    );
};

export default ChartDonut;
