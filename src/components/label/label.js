import React from "react";
import "./label.scss";

const Label = ({ text }) => {
    return <div className="labelButton">{text}</div>;
};

export default Label;
