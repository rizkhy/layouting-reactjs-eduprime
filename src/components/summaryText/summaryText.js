import React from "react";
import "./summaryText.scss";

const SummaryText = ({ title, title1, value, value1, display, float }) => {
    return (
        <div style={{ display: display, float: float }}>
            <div className="summaryTitle">{title}</div>
            <div className="summaryTitle1">{title1}</div>
            <div className="summaryValue">{value}</div>
            <div className="summaryValue">{value1}</div>
        </div>
    );
};

export default SummaryText;
