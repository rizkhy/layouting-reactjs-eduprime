import React from "react";
import "./labelPsychology.scss";

const LabelPsychology = ({ image, width, title, subValue, subTitle }) => {
    return (
        <div>
            <div className="labelPsychology">
                <div className="labelIcon">
                    <img src={image} width={width} />
                </div>
                <div className="label">
                    <div className="labelTitle">{title}</div>
                    <div className="labelValue">{subValue}</div>
                    <div className="labelDesc">{subTitle}</div>
                </div>
            </div>
        </div>
    );
};

export default LabelPsychology;
