import React from "react";
import "./labelSection.scss";

const LabelSection = ({ image, width, title, subTitle }) => {
    return (
        <div>
            <div className="labelSection">
                <div className="labelIcon">
                    <img src={image} width={width} />
                </div>
                <div className="label">
                    <div className="labelTitle">{title}</div>
                    <div className="labelDesc">{subTitle}</div>
                </div>
                <hr
                    style={{
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                    }}
                />
            </div>
        </div>
    );
};

export default LabelSection;
