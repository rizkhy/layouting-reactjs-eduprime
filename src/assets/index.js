// icon
import Clipboard from "./icon/clipboard.svg";
import IcChart from "./icon/chart.svg";
import IcHeadphone from "./icon/headphone.svg";
import IcBinocular from "./icon/binochular.svg";
import IcPerson from "./icon/person.svg";

// image
import AGCU from "./image/agcu.png";
import Logo from "./image/jatim.png";

export { Clipboard, AGCU, Logo, IcChart, IcHeadphone, IcBinocular, IcPerson };
