import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { PageOne, PageTwo } from "../../pages";

const Router = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<PageOne />} />
            </Routes>
            <Routes>
                <Route path="/page-two" element={<PageTwo />} />
            </Routes>
        </BrowserRouter>
    );
};

export default Router;
