import React from "react";
import { AGCU, Clipboard, IcChart, Logo } from "../../assets";
import {
    ChartDonut,
    Dot,
    Gap,
    Label,
    LabelSection,
    SummaryText,
} from "../../components";
import "./pageOne.scss";

const PageOne = () => {
    return (
        <div>
            <div className="header">
                <div className="logo">
                    <img src={Logo} width={100} />
                </div>
                <div className="title">
                    <div className="title1">BENCHMARKING COMPREHENSIVE</div>
                    <div className="title2">
                        ACADEMIC GENERAL CHECK UP (C'AGCU)
                    </div>
                    <div className="title3">
                        DINAS PENDIDIKAN PROVINSI JAWA TIMUR
                    </div>
                </div>
                <div className="right-logo">
                    <img src={AGCU} width={230} />
                </div>
            </div>
            <div className="container">
                <LabelSection
                    image={Clipboard}
                    width={50}
                    title="KOMPOSISI TEST C' AGCU"
                    subTitle="DINAS PENDIDIKAN JAWA TIMUR"
                />
                <Gap height={80} />
                <hr
                    style={{
                        width: "90%",
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                        float: "left",
                    }}
                />
                <div className="bullet">
                    <div className="bullet1">
                        <Dot />
                    </div>
                    <div className="bullet2">
                        <Dot />
                    </div>
                    <div className="bullet3">
                        <Dot />
                    </div>
                </div>
                <br />
                <div className="summaryText">
                    <div className="summary1">
                        <SummaryText
                            title={"KOGNITIF"}
                            value={"4 Mata Pelajaran"}
                        />
                    </div>
                    <div className="summary2">
                        <SummaryText
                            title={"LEARNING STYLE"}
                            value={"48 Butir Soal"}
                        />
                    </div>
                    <div className="summary3">
                        <SummaryText
                            title={"PSYCHOLOGY TEST"}
                            value={"24 Butir Soal"}
                        />
                    </div>
                </div>
                <hr
                    style={{
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                    }}
                />
                <Gap height={40} />
                <div className="school">
                    <div className="left">
                        <LabelSection
                            image={IcChart}
                            width={50}
                            title="DATA PENDIDIKAN SMA NEGERI"
                            subTitle="PROVINSI JAWA TIMUR"
                        />

                        <Gap height={20} />
                        <SummaryText
                            title={"JUMLAH WILAYAH"}
                            value={"38 Kabupaten/Kota"}
                            display={"inline-block"}
                        />

                        <SummaryText
                            title={"SATUAN PENDIDIKAN"}
                            value={"423 Sekolah"}
                            display={"inline-block"}
                            float={"right"}
                        />
                        <hr
                            style={{
                                backgroundColor: "lightgrey",
                                border: "1px solid lightgrey",
                                height: "1px",
                            }}
                        />
                        <Gap height={20} />

                        <SummaryText
                            title={"JUMLAH Siswa"}
                            value={"407.258 Siswa"}
                        />
                        <hr
                            style={{
                                backgroundColor: "lightgrey",
                                border: "1px solid lightgrey",
                                height: "1px",
                            }}
                        />
                        <Gap height={30} />
                    </div>
                    <div className="right">
                        <div style={{ width: "60%", marginLeft: "25%" }}>
                            <ChartDonut
                                chartDataVal={[40, 60]}
                                bgColorVal={["#edb04e", "#bdbdbd"]}
                            />
                            <p className="chartValue">84,21%</p>
                        </div>
                        <SummaryText
                            title={"100% SELESAI MENGERJAKAN"}
                            value={"32 Kabupaten/Kota"}
                        />
                    </div>
                </div>
                <hr
                    style={{
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                    }}
                />
                <Gap height={20} />

                <div className="information">
                    <div className="leftInformation">
                        <Label text={"INFORMASI HASIL PEMETAAN KOGNITIF"} />

                        <div style={{ width: "100%" }}>
                            <div
                                style={{
                                    width: "40%",
                                    display: "inline-block",
                                }}
                            >
                                <ChartDonut
                                    chartDataVal={[30, 10]}
                                    bgColorVal={["#61d09e", "#bdbdbd"]}
                                />
                                <p className="chartValue1">56,45%</p>
                            </div>
                            <div
                                style={{
                                    width: "49%",
                                    display: "inline-block",
                                    position: "absolute",
                                    marginTop: "10%",
                                }}
                            >
                                <SummaryText title={"RATA-RATA PROVINSI"} />
                            </div>
                        </div>

                        <div style={{ width: "100%" }}>
                            <div
                                style={{
                                    width: "40%",
                                    display: "inline-block",
                                }}
                            >
                                <ChartDonut
                                    chartDataVal={[70, 10]}
                                    bgColorVal={["#fd521a", "#bdbdbd"]}
                                />
                                <p className="chartValue2">60,23%</p>
                            </div>
                            <div
                                style={{
                                    width: "49%",
                                    display: "inline-block",
                                    position: "absolute",
                                    marginTop: "8%",
                                }}
                            >
                                <SummaryText
                                    title={"SEBARAN RATA-RATA"}
                                    title1={"NILAI TERBANYAK"}
                                    value={"Rata-rata nilai siswa mayoritas"}
                                    value1={"berada pada kisaran 55,00-60,00"}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="rightInformation">
                        <div
                            style={{
                                width: "60%",
                                marginLeft: "25%",
                                marginBottom: "30px",
                            }}
                        >
                            <ChartDonut
                                chartDataVal={[90, 60]}
                                bgColorVal={["#1e3163", "#bdbdbd"]}
                            />
                            <p className="chartValue4">26,32%</p>
                        </div>

                        <SummaryText
                            title={"DI ATAS RATA-RATA PROVINSI"}
                            value={"10 Cabdindik"}
                            value1={"10 Kabupaten/Kota"}
                        />
                        <hr
                            style={{
                                backgroundColor: "lightgrey",
                                border: "1px solid lightgrey",
                                height: "1px",
                            }}
                        />
                        <Gap height={10} />
                        <SummaryText
                            title={"SATUAN PENDIDIKAN"}
                            title1={"54,84%"}
                            value={"232 Sekolah"}
                        />
                        <hr
                            style={{
                                backgroundColor: "lightgrey",
                                border: "1px solid lightgrey",
                                height: "1px",
                            }}
                        />
                        <Gap height={10} />
                        <SummaryText
                            title={"JUMLAH SISWA"}
                            title1={"62%"}
                            value={"239.876 Siswa"}
                        />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PageOne;
