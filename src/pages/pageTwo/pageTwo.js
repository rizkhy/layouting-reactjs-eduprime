import React from "react";
import { IcBinocular, IcHeadphone, IcPerson } from "../../assets";
import {
    ChartDonut,
    Gap,
    Label,
    LabelPsychology,
    SummaryText,
} from "../../components";
import "./pageTwo.scss";

const PageTwo = () => {
    return (
        <div>
            <div className="container">
                <Gap height={80} />

                <div className="school">
                    <div className="left">
                        <div style={{ width: "60%", marginLeft: "25%" }}>
                            <ChartDonut
                                chartDataVal={[50, 50, 100]}
                                bgColorVal={["#edb04e", "#fd521a", "#33cddc"]}
                            />
                        </div>
                    </div>
                    <div className="right">
                        <div style={{ marginTop: "20px" }} />
                        <SummaryText
                            title={"KATEGORI MURAH"}
                            value={
                                "15,35 % atau 59.388 siswa, soal dalam kategori Mudah."
                            }
                        />
                        <div style={{ marginTop: "20px" }} />
                        <SummaryText
                            title={"KATEGORI SEDANG"}
                            value={
                                "19,55 % atau 75.638 siswa, soal dalam kategori Sedang."
                            }
                        />
                        <div style={{ marginTop: "20px" }} />
                        <SummaryText
                            title={"KATEGORI SULIT"}
                            value={
                                "65,1% atau 251.869 Siswa, soal dalam kategori Sulit."
                            }
                        />
                    </div>
                </div>

                <Gap height={40} />

                <div className="mataPelajaran">
                    <div className="leftMP">
                        <SummaryText
                            title={"MATA PELAJARAN POTENSI"}
                            value={
                                "*Mata pelajaran potensi adalah mata pelajaran"
                            }
                            value1={"yang rata-rata nilai pada kabupaten >= 70"}
                        />
                        <div style={{ marginTop: "40px" }} />
                        <div
                            style={{
                                width: "50%",
                                fontStyle: "italic",
                                fontSize: "25px",
                                display: "inline-block",
                            }}
                        >
                            Biologi
                        </div>
                        <div
                            style={{
                                width: "50%",
                                fontWeight: "800",
                                fontSize: "30px",
                                display: "inline-block",
                            }}
                        >
                            42,15%
                        </div>
                    </div>
                    <div className="rightMP">
                        <SummaryText
                            title={"MATA PELAJARAN DARURAT"}
                            value={
                                "Mata pelajaran darurat adalah mata pelajaran yang"
                            }
                            value1={"rata-rata nilai pada kabupaten <= 50"}
                        />
                        <Gap height={40} />
                        <div
                            style={{
                                width: "50%",
                                fontStyle: "italic",
                                fontSize: "25px",
                                display: "inline-block",
                            }}
                        >
                            Fisika
                        </div>
                        <div
                            style={{
                                width: "50%",
                                fontWeight: "800",
                                fontSize: "30px",
                                display: "inline-block",
                            }}
                        >
                            76,35%
                        </div>
                    </div>
                </div>

                <Gap height={40} />

                <div style={{ width: "70%" }}>
                    <Label text={"INFORMASI HASIL PEMETAAN LEARNING STYLE"} />
                </div>
                <Gap height={10} />
                <hr
                    style={{
                        width: "90%",
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                        float: "left",
                    }}
                />
                <Gap height={40} />
                <div
                    style={{
                        width: "30%",
                        display: "inline-block",
                        marginLeft: "5%",
                    }}
                >
                    <LabelPsychology
                        image={IcBinocular}
                        width={100}
                        title="42%"
                        subValue="171.048 Siswa"
                        subTitle="VISUAL"
                    />
                </div>
                <div style={{ width: "30%", display: "inline-block" }}>
                    <LabelPsychology
                        image={IcHeadphone}
                        width={100}
                        title="37%"
                        subValue="150.685 Siswa"
                        subTitle="AUDITORI"
                    />
                </div>
                <div style={{ width: "30%", display: "inline-block" }}>
                    <LabelPsychology
                        image={IcPerson}
                        width={100}
                        title="21%"
                        subValue="85.524 Siswa"
                        subTitle="KINESTETIK"
                    />
                </div>
                <Gap height={40} />

                <div style={{ width: "70%" }}>
                    <Label text={"INFORMASI HASIL PEMETAAN PSYCHOLOGY TEST"} />
                </div>
                <Gap height={10} />
                <hr
                    style={{
                        width: "90%",
                        backgroundColor: "lightgrey",
                        border: "1px solid lightgrey",
                        height: "1px",
                        float: "left",
                    }}
                />
                <div style={{ width: "100%" }}>
                    <div style={{ width: "30%", display: "inline-block" }}>
                        <div
                            style={{
                                width: "80%",
                                marginLeft: "25%",
                            }}
                        >
                            <ChartDonut
                                chartDataVal={[70, 90, 10]}
                                bgColorVal={["#edb04e", "#fd521a", "#33cddc"]}
                            />
                            <p
                                style={{
                                    textAlign: "center",
                                    fontSize: "20px",
                                    fontWeight: "600",
                                }}
                            >
                                ADEVERSITY QUOTIENT
                            </p>
                        </div>
                    </div>
                    <div style={{ width: "30%", display: "inline-block" }}>
                        <div
                            style={{
                                width: "80%",
                                marginLeft: "25%",
                            }}
                        >
                            <ChartDonut
                                chartDataVal={[40, 20, 60]}
                                bgColorVal={["#edb04e", "#fd521a", "#33cddc"]}
                            />
                            <p
                                style={{
                                    textAlign: "center",
                                    fontSize: "20px",
                                    fontWeight: "600",
                                }}
                            >
                                EMOTIONAL QUOTIENT
                            </p>
                        </div>
                    </div>
                    <div style={{ width: "30%", display: "inline-block" }}>
                        <div
                            style={{
                                width: "80%",
                                marginLeft: "25%",
                            }}
                        >
                            <ChartDonut
                                chartDataVal={[70, 30, 100]}
                                bgColorVal={["#edb04e", "#fd521a", "#33cddc"]}
                            />
                            <p
                                style={{
                                    textAlign: "center",
                                    fontSize: "20px",
                                    fontWeight: "600",
                                }}
                            >
                                ACHIEVEMENT MOTIVATION
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default PageTwo;
